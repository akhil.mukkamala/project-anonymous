import { Injectable } from '@angular/core';
import * as io from 'socket.io-client';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SocketsService {
    socket: any;
    readonly uri = 'http://localhost:8081'
     
  constructor() { 
      this.socket = io(this.uri)
  }

  listen(eventName: string) {
      return new Observable((subscriber) => {
          this.socket.on(eventName, (data) => {
              subscriber.next(data);
          })
      })
  }

  emit(eventName, data) {
      this.socket.emit(eventName, data);
  }

}
