import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import { AuthService } from './../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  constructor(
    private toastr: ToastrService,
    private auth: AuthService,
    private router: Router
  ) {}

  loginform = true;
  recoverform = false;

  ngOnInit() {

  }
  
  showRecoverForm() {
    this.loginform = !this.loginform;
    this.recoverform = !this.recoverform;
  }

  logMeIn(username: String, password: String) {
    const creds = {
      username: username,
      password: password
    };
    this.auth.login(creds).subscribe(
      data => {
        this.toastr.success('Logged In', 'Succesfull');
        localStorage.setItem('authtoken', data.authtoken);
        // localStorage.setItem('permission', data.permission);
        this.router.navigateByUrl('/dashboard/home');
      },
      err => {
        console.log('err', err);
        if (err.error.authorization == false && err.status == 400) {
            this.toastr.error(
                'Please check username & password',
                'Details Incorrect'
              );
          // this.router.navigateByUrl('/bad-request');
        } else {
            this.toastr.error('Error logging In', 'Bad Request');
        }
      }
    );
  }
}
