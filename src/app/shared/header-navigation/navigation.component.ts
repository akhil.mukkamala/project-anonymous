import { Component, AfterViewInit, OnInit, EventEmitter, Output } from '@angular/core';
import {
  NgbModal,
  ModalDismissReasons,
  NgbPanelChangeEvent,
  NgbCarouselConfig
} from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { YoutubeService } from './../../services/youtube.service';
import { DatashareService } from 'src/app/services/datashare.service';

declare var $: any;

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  @Output() toggleSidebar = new EventEmitter<void>();

  public config: PerfectScrollbarConfigInterface = {};

  public showSearch = false;
  data: any;
  domain: { 'domain': any; };

  constructor(private router: Router, private modalService: NgbModal, private datashare: DatashareService, private ytService: YoutubeService) {}

  ngOnInit() {
    //   if(this.router.url == '/' || this.router.url == '/dashboard/home') {
    //     console.log(this.router.url);
    //     this.showSearch = true;
    //   }
  }

 


  getYouTubeUrl(youTubeUrl) {
      let url = { youTubeUrl };
      this.domain = {'domain': url.youTubeUrl};
      return this.datashare.sendData(this.domain);
  }
}

