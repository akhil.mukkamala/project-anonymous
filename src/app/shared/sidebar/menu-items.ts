import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
  {
    path: '',
    title: 'Explore',
    icon: 'mdi mdi-dots-horizontal',
    class: 'nav-small-cap',
    extralink: true,
    submenu: []
  },
  {
    path: '/dashboard/home',
    title: 'Home',
    icon: 'mdi mdi-adjust',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/domain-management',
    title: 'Domain Management',
    icon: 'mdi mdi-adjust',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/uptime-monitor',
    title: 'Uptime Monitor',
    icon: 'mdi mdi-adjust',
    class: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/sockets',
    title: 'Sockets',
    icon: 'mdi mdi-adjust',
    class: '',
    extralink: false,
    submenu: []
  },
    {
    path: '/dashboard/mvp',
    title: 'MVP',
    icon: 'mdi mdi-adjust',
    class: '',
    extralink: false,
    submenu: []
  },
  //   {
  //     path: '',
  //     title: 'Dashboards',
  //     icon: 'mdi mdi-view-dashboard',
  //     class: 'has-arrow',
  //     extralink: false,
  //     submenu: [
  //       {
  //         path: '/dashboard/home',
  //         title: 'Home',
  //         icon: 'mdi mdi-adjust',
  //         class: '',
  //         extralink: false,
  //         submenu: []
  //       },
  //     ]
  //   }
];
