import { NgModule, Injector } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { createCustomElement } from '@angular/elements';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule } from 'ng2-charts';
import { ChartistModule } from 'ng-chartist';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { CalendarModule, CalendarDateFormatter } from 'angular-calendar';
import { MatVideoModule } from 'mat-video';
import {MatChipsModule} from '@angular/material/chips';
import {CrystalGalleryModule} from 'ngx-crystal-gallery';
import { TableModule } from 'primeng/table';
import {DialogModule} from 'primeng/dialog';

import {VgCoreModule} from 'videogular2/core';
import {VgControlsModule} from 'videogular2/controls';
import {VgOverlayPlayModule} from 'videogular2/overlay-play';
import {VgBufferingModule} from 'videogular2/buffering';

import { DashboardRoutes } from './dashboard.routing';

import { Dashboard1Component } from './dashboard1/dashboard1.component';
import { DomainNotifierComponent } from './domain-notifier/domain-notifier.component';
import { UptimerMonitorComponent } from './uptimer-monitor/uptimer-monitor.component';
import { DomainManagementComponent } from './domain-management/domain-management.component';
import {InputSwitchModule} from 'primeng/inputswitch';
import { SocketsComponent } from './sockets/sockets.component';
import { MvpComponent } from './mvp/mvp.component';

import { SearchPipe } from './../pipes/search.pipe';
import { SortPipe } from './../pipes/sort.pipe'

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    NgbModule,
    ChartsModule,
    ChartistModule,
    MatVideoModule,
    MatChipsModule,
    CrystalGalleryModule,
    VgCoreModule,
    VgControlsModule,
    VgOverlayPlayModule,
    VgBufferingModule,
    RouterModule.forChild(DashboardRoutes),
    PerfectScrollbarModule,
    CalendarModule.forRoot(),
    NgxChartsModule,
    NgxDatatableModule,
    TableModule,
    DialogModule,
    InputSwitchModule
  ],
  declarations: [
    Dashboard1Component,
    DomainNotifierComponent,
    UptimerMonitorComponent,
    DomainManagementComponent,
    SocketsComponent,
    MvpComponent,
    SearchPipe,
    SortPipe,
  ],
  exports: [
      SearchPipe,
      SortPipe,
  ]
})
export class DashboardModule {
}
