import { Component, AfterViewInit, OnInit } from '@angular/core';
import { YoutubeService } from './../../services/youtube.service';
import { CrystalLightbox } from 'ngx-crystal-gallery';
import { DatashareService } from 'src/app/services/datashare.service';


@Component({
  templateUrl: './dashboard1.component.html',
  styleUrls: ['./dashboard1.component.css']
})
export class Dashboard1Component implements OnInit {
    reactionCount: any;
    commentCount: any;
    shareCount: any;
    commentPluginCount: any;

    constructor(private datashare: DatashareService, private ytService: YoutubeService, public lightbox: CrystalLightbox) { }
  
    viewThumb: boolean = false;
    videoTitle: any = '';
    subscribers: any;
    videoInfo: any;
    channelInfo: any;
    channelViews: any;
    likes: any;
    comments: any;
    videoViews: any;
    duration: any;
    uploads: any;
    dislikes: any;
    videoDesc: any;
    tags: any;
    videoPublished: any;
    vidThumbnail: any;
    captions: any;
    vidThumbnails: any = [];
    liveStreamingStatus: boolean;
    liveStreamingStartTime: any;
    liveStreamingEndTime: any;
    liveStreamingScheduledTime: any;
    vidURL: any;


    // channelInfo: any;
    channelID: any;
    channelTitle: any;
    channelGA: any;
    channelTopics: any;
    channelCountry: any;
    channelPublished: any;
    channelDesc: any;
    channelThumbs: any;
    channelThumbsArr: any = [];
    channelTopicsArr: any = [];
    firstChannelThumb: any = 'http://www.gravatar.com/avatar/3b3be63a4c2a439b013787725dfce802?d=identicon';
    channelUrl: any;
    cmpltChannelUrl: any;

    // https://www.youtube.com/watch?v=7JA90VI9fAI

  ngOnInit() {
    // this.ytService.testService().subscribe((data) => {
    //     console.log('tstdata', data);
    // })
    this.datashare.listen().subscribe((url: any) => {
           this.ytService.youtubeMetrics(url).subscribe((data) => {
               console.log('data', data)
            this.vidThumbnails = [];
            if (data) {
                this.vidURL = data.url
                this.viewThumb = true;
                this.channelInfo = data.channelInfo;
                this.videoInfo = data.videoInfo;
                this.videoTitle = this.videoInfo.titleMeta.videoTitle;
                this.subscribers = this.videoInfo.statistics.channelSubs;
                this.channelViews = this.channelInfo.statistics.channelViews;
                this.likes = this.videoInfo.statistics.likeCount;
                this.comments = this.videoInfo.statistics.commentCount;
                this.videoViews = this.videoInfo.statistics.viewCount;
                this.dislikes = this.videoInfo.statistics.dislikes;
                this.duration = this.videoInfo.statistics.duration;
                this.videoDesc = this.videoInfo.titleMeta.videoDesc;
                this.uploads = this.videoInfo.statistics.uploads;
                this.tags = this.videoInfo.titleMeta.videoTags;
                this.videoPublished = this.videoInfo.titleMeta.videoPublished;
                this.vidThumbnail = this.videoInfo.titleMeta.videoThumbnailURL;
                this.liveStreamingStatus = this.videoInfo.liveStreamDetails.liveStreamingStatus;
                this.liveStreamingStartTime = this.videoInfo.liveStreamDetails.liveStreamingStartTime;
                this.liveStreamingEndTime = this.videoInfo.liveStreamDetails.liveStreamingEndTime;
                this.liveStreamingScheduledTime = this.videoInfo.liveStreamDetails.liveStreamingScheduledTime;

                this.channelID = this.channelInfo.titleMeta.channelId;
                this.channelTitle = this.channelInfo.titleMeta.channelTitle;
                this.channelDesc = this.channelInfo.titleMeta.channelDesc;
                this.channelPublished = this.channelInfo.titleMeta.channelPublished;
                this.channelCountry = this.channelInfo.titleMeta.channelCountry;
                this.channelTopics = this.channelInfo.titleMeta.channelTopics;
                this.channelGA = this.channelInfo.titleMeta.channelGA;
                this.channelThumbs = this.channelInfo.titleMeta.channelThumbs;
                this.channelUrl = this.channelInfo.titleMeta.channelUrl;
                this.cmpltChannelUrl = 'https://youtube.com/' + this.channelUrl;
                this.firstChannelThumb = this.channelInfo.titleMeta.channelThumbs[0];

                for (var i = 0; i < this.channelTopics.length; i++) {
                    this.channelTopicsArr.push(this.channelTopics[i])
                }

                for (var i = 0; i < this.channelThumbs.length; i++) {
                    this.channelThumbsArr.push(this.channelThumbs[i])
                }

                for (var i = 0; i < this.vidThumbnail.length; i++) {
                    this.vidThumbnails.push(
                        {
                            preview: this.vidThumbnail[i],
                            full: this.vidThumbnail[i]
                        }
                    )
                }
                this.captions = this.videoInfo.statistics.captionStatus;
            } else {
                console.log('No data from backend');
            }
        })

        this.ytService.facebookMetrics(url).subscribe((data) => {
            if (data) {
                let response = data.response.engagement;
                this.reactionCount = response.reaction_count;
                this.commentCount = response.comment_count;
                this.shareCount = response.share_count;
                this.commentPluginCount = response.comment_plugin_count;
            } else {
                console.log('No data from backend')
            }
        })

        

    })
  }

}
