import { Component, OnInit } from '@angular/core';
import { MvpService } from 'src/app/services/mvp.service';
import { SearchPipe } from 'src/app/pipes/search.pipe';
import { SortPipe } from 'src/app/pipes/sort.pipe';
import { PaginationService } from 'src/app/services/pagination.service';

@Component({
  selector: 'app-mvp',
  templateUrl: './mvp.component.html',
  styleUrls: ['./mvp.component.css']
})
export class MvpComponent implements OnInit {
  //   constructor(private mvpService: MvpService) { }
  data: any;

  private allItems: any[];

  pager: any = {};
  constructor(
    private mvpService: MvpService,
    private PaginationService: PaginationService
  ) {}

  //  This is Just Raw Code! For Functionality Purpose, I always use types and models for all Projects

  pagedItems: any[];
  ngOnInit() {
    this.mvpService
      .getData()
      .map(response => response)
      .subscribe(data => {
        this.allItems = data;
        console.log(this.allItems);

        this.setPage(1);
      });
  }

  setPage(page: number) {
    this.pager = this.PaginationService.getPager(this.allItems.length, page);
    this.pagedItems = this.allItems.slice(
      this.pager.startIndex,
      this.pager.endIndex + 1
    );
  }
}
