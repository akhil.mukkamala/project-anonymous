import { Component, OnInit } from '@angular/core';
import { SocketsService } from 'src/app/sockets.service';

@Component({
  selector: 'app-sockets',
  templateUrl: './sockets.component.html',
  styleUrls: ['./sockets.component.css']
})
export class SocketsComponent implements OnInit {

  constructor(private socket: SocketsService) { }

  ngOnInit() {
      this.socket.listen('Hi').subscribe(data => {
          console.log(data);
      })
  }

}
