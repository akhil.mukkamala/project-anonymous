import { Routes } from '@angular/router';

import { Dashboard1Component } from './dashboard1/dashboard1.component';
import { DomainNotifierComponent } from './domain-notifier/domain-notifier.component';
import { UptimerMonitorComponent } from './uptimer-monitor/uptimer-monitor.component';
import { DomainManagementComponent } from './domain-management/domain-management.component';
import { SocketsComponent } from './sockets/sockets.component';
import { MvpComponent } from './mvp/mvp.component';


export const DashboardRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'home',
        component: Dashboard1Component,
        data: {
          title: 'Home',
          urls: [
            { title: 'Dashboard', url: '/dashboard' },
            { title: 'Home' }
          ]
        }
      },
      {
        path: 'domain-management',
        component: DomainManagementComponent,
        data: {
          title: 'Domain Management',
          urls: [
            { title: 'Dashboard', url: '/dashboard' },
            { title: 'Domain Management' }
          ]
        }
      },
      {
        path: 'uptime-monitor',
        component: UptimerMonitorComponent,
        data: {
          title: 'Uptime Monitor',
          urls: [
            { title: 'Dashboard', url: '/dashboard' },
            { title: 'Uptime Monitor' }
          ]
        }
      },
      {
        path: 'sockets',
        component: SocketsComponent,
        data: {
          title: 'Sockets',
          urls: [
            { title: 'Dashboard', url: '/dashboard' },
            { title: 'Sockets' }
          ]
        }
      },
            {
        path: 'mvp',
        component: MvpComponent,
        data: {
          title: 'mvp',
          urls: [
            { title: 'Dashboard', url: '/dashboard' },
            { title: 'MVP' }
          ]
        }
      }
    ]
  }
];
