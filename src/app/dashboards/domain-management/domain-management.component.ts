import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-domain-management',
  templateUrl: './domain-management.component.html',
  styleUrls: ['./domain-management.component.css']
})
export class DomainManagementComponent implements OnInit {



  displayDialog: boolean;

  domainInfo = {};

  selectedDomain;

  newDomain: boolean;

  listDomainsData;

  cols: any[];

  constructor () {}

  ngOnInit() {
      this.cols = [
          { field: 'name', header: 'Name' },
          { field: 'hosting', header: 'Hosting' },
          { field: 'expiry', header: 'Expiry Date' }
      ];

      this.listDomainsData = [
    {
        name: 'https://akhil.com',
        hosting: 'Google',
        expiry: '2019-12-02',
    },
    {
        name: 'https://developerfox.com',
        hosting: 'Google',
        expiry: '2019-12-02',
    },
    {
        name: 'https://akhil.com',
        hosting: 'Google',
        expiry: '2019-12-02',
    },
    {
        name: 'https://akhil.com',
        hosting: 'Google',
        expiry: '2019-12-02',
    }
    ]
  }

  showDialogToAdd() {
      this.newDomain = true;
      this.domainInfo = {};
      this.displayDialog = true;
  }

  save() {
      let listDomainsData = [...this.listDomainsData];
      if (this.newDomain)
          listDomainsData.push(this.domainInfo);
      else
          listDomainsData[this.listDomainsData.indexOf(this.selectedDomain)] = this.domainInfo;

      this.listDomainsData = listDomainsData;
      this.domainInfo = null;
      this.displayDialog = false;
  }

  delete() {
      let index = this.listDomainsData.indexOf(this.selectedDomain);
      this.listDomainsData = this.listDomainsData.filter((val, i) => i != index);
      this.domainInfo = null;
      this.displayDialog = false;
  }

  onRowSelect(event) {
      this.newDomain = false;
      this.domainInfo = this.cloneCar(event.data);
      this.displayDialog = true;
  }

  cloneCar(c) {
      let domainInfo = {};
      for (let prop in c) {
        domainInfo[prop] = c[prop];
      }
      return domainInfo;
  }

}
