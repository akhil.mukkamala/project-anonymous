import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {
  NG_VALIDATORS,
  Validator,
  ValidationErrors,
  FormGroup
} from '@angular/forms';

@Component({
  selector: 'app-domain-notifier',
  templateUrl: './domain-notifier.component.html',
  styleUrls: ['./domain-notifier.component.css']
})
export class DomainNotifierComponent implements OnInit {
  // model: any = {};
  @ViewChild('f') formValues; // Added this

  notificationDetails: any;
  showPopup: any;
  checkChanged: any;
  headerTitle: string;
  info: any = {};
  editedIndex: any;
  domains: string[];
  hostingProvider;
  expiryDate;
  domainName;
  remindStatus;
  newDetails: any = {};
  // display: 'none'
  constructor() {}

  ngOnInit() {
    this.domains = [
      '1&1',
      'AWS',
      'Bluehost',
      'Dreamhost',
      'GoDaddy',
      'Google',
      'Hostgator',
      'Namecheap'
    ];
    this.showPopup = false;
    this.notificationDetails = [
      {
        domainName: 'https://developerfox.com',
        hostingProvider: 'GoDaddy',
        expiryDate: '20-09-2019',
        remindStatus: true
      },
      {
        domainName: 'https://thetrail.com',
        hostingProvider: 'AWS',
        expiryDate: '20-09-2019',
        remindStatus: false
      },
      {
        domainName: 'https://example.com',
        hostingProvider: 'Bluehost',
        expiryDate: '20-09-2019',
        remindStatus: true
      }
    ];
  }

  openDomainPopup() {
    this.info = {};
    this.headerTitle = 'Add New Domain';
    this.showPopup = true;
  }

  closeDomainPopup() {
    this.showPopup = false;
  }

  editDomainPopup(details) {
    this.headerTitle = 'Edit Details';
    this.showPopup = true;
    this.newDetails = details;
  }

  captureCategorySelect(event) {}

  submitDomainDetails() {
    this.showPopup = false;
    console.log(this.newDetails);
  }
}
