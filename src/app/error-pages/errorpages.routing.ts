import { Routes } from '@angular/router';

import { NotfoundComponent } from './404/not-found.component';

export const ErrorPagesRouting: Routes = [
  {
    path: '',
    children: [
      {
        path: '404',
        component: NotfoundComponent
      }
    ]
  }
];
