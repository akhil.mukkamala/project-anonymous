import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { NotfoundComponent } from './404/not-found.component';

import { ErrorPagesRouting } from './errorpages.routing';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ErrorPagesRouting),
    NgbModule
  ],
  declarations: [
    NotfoundComponent
  ]
})
export class ErrorPagesModule {}
