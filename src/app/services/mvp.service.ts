import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class MvpService {
  constructor(private http: HttpClient) {}

  private baseApi = environment.baseUrl;

  getData(): Observable<any> {
    return this.http.get(this.baseApi).map(res => res);
  }
}
