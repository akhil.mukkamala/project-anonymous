import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import { environment } from './../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class AuthService {

  private apiUrl = environment.apiURL;
  constructor(private http: HttpClient) {}

  login(creds): Observable<any> {
    return this.http.post(this.apiUrl + '/login', creds).map(res => res);
  }
  signUp(user): Observable<any> {
    return this.http.post(this.apiUrl + '/signup', user).map(res => res);
  }

  getToken() {
    return localStorage.getItem('authtoken');
  }

  logout() {
    return localStorage.removeItem('authtoken');
  }

}
