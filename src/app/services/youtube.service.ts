import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from './../../environments/environment';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';

import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class YoutubeService {

  private apiUrl = environment.apiURL;
    
  constructor(private http: HttpClient) { }

  youtubeMetrics(youTubeUrl): Observable<any>  {
    return this.http.post(this.apiUrl + '/youtube-metrics', youTubeUrl).map(res => res);
  }

  facebookMetrics(url): Observable<any>  {
    return this.http.post(this.apiUrl + '/facebook-metrics', url).map(res => res);
  }

  testService(): Observable<any> {
      return this.http.get(this.apiUrl + '/test').map(res => res);
  }

}
