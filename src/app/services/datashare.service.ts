import { Injectable } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable({
  providedIn: 'root'
})
export class DatashareService {

  constructor() { }

  private data = new Subject<any>();

  public listen(): Observable<any> {
      return this.data.asObservable();
  }

  public sendData(data) {
      this.data.next(data);
  }

  private showSearch = new BehaviorSubject(true);
  currentStatus = this.showSearch.asObservable();

  changeStatus(status: boolean) {
    this.showSearch.next(status)
  }

}
