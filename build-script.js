const fs = require('fs-extra');
const concat = require('concat');
(async function build() {
    const files = [
        './dist/analyse-metrics/runtime.js',
        './dist/analyse-metrics/polyfills.js',
        './dist/analyse-metrics/scripts.js',
        './dist/analyse-metrics/main.js',
    ]
    await fs.ensureDir('elements')
    await concat(files, 'elements/feedback-poll.js');
    console.info('Elements created successfully!')
})()